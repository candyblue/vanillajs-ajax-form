var app = document.querySelector('#app'),
    contactForm = document.querySelector('#contactForm'),
    contactFormHny = document.querySelector('#hny'),
    contactFormAlert = document.querySelector('#contactFormAlert');

var webhook = 'https://hooks.zapier.com/hooks/catch/******', // <-- paste in your custom Webhook-URL
    successMessage = 'Yay! Your message was sent successfully.',
    errorMessage = 'An error occurred, please write us an email.';

document.addEventListener('DOMContentLoaded', function() {
  app.classList.remove('no-js');
  app.classList.add('js');

  contactForm.onsubmit = function (e) {
    e.preventDefault();

    if (contactFormHny.value.length == 0) {
      var data = {};
      for (var i = 0, ii = contactForm.length; i < ii; ++i) {
        var input = contactForm[i];
        if (input.name) {
        	data[input.name] = input.value;
        }
      }

      var xhr = new XMLHttpRequest();
      xhr.open('POST', webhook, true);
      xhr.send(JSON.stringify(data));
      xhr.onloadend = function() {
        contactForm.reset();
        contactFormAlert.insertAdjacentHTML( 'beforeend', successMessage );
      };
    } else {
      contactFormAlert.insertAdjacentHTML( 'beforeend', errorMessage );
    }
  };
})
