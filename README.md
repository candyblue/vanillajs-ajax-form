# Send form data as email using Zapier</h1>
 Static Websites are lit, but in most cases there are no server-side-languages, as PHP, needed to send a form for example. There are a number of paid services, such as formspree, but we wanted a free, highly customizable und scalable solution.

With this vanillaJS-only solution you can send emails without server-side-languages, as PHP. Our script is designed to work with Zapier.
The existing solutions we found are based on jquery, so we made it without jquery.
VanillaJS AJAX form for sending emails with Zapier

We've written a blog-post with some more details (in German).

# Usage
just copy the files into your project and build your own Zap on (zapier.com)
